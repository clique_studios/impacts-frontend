/** import external dependencies */
import $ from 'jquery/dist/jquery.js';
import "waypoints/lib/jquery.waypoints.min";
import Swiper from 'swiper/dist/js/swiper';
import "prcomboselect";

/** import local dependencies */
import Router from './util/Router';
import common from './routes/common';
import pageTemplateTemplateHome from './routes/home';
import pageTemplateTemplateAbout from './routes/about';
import pageTemplateTemplateApproach from './routes/approach';
import pageTemplateTemplateResources from './routes/resources';
import single from './routes/post';
import pageTemplateTemplateServices from './routes/services';
import pageTemplateTemplateContact from './routes/contact';
import error404 from './routes/error404';

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
    /** All pages */
    common,
    /** Home page */
    pageTemplateTemplateHome,
    pageTemplateTemplateAbout,
    pageTemplateTemplateApproach,
    pageTemplateTemplateResources,
    single,
    pageTemplateTemplateServices,
    pageTemplateTemplateContact,
    error404,
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());