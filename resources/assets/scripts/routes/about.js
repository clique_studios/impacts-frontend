import TweenMax from "gsap/src/minified/TweenMax.min";

export default {
    init() {

    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS
        //plot graph hover
        if ($(".plot-graph").length > 0) {
            $(".plot-graph").each(function () {
                var graph = $(this);

                graph.find("circle").each(function () {
                    var dot = $(this);
                    var random_x = Math.floor(Math.random() * 40) + 20;
                    random_x *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;
                    var random_y = Math.floor(Math.random() * 40) + 20;
                    random_y *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;

                    graph.on("mousemove", function (e) {
                        var dot_x = dot[0].getBoundingClientRect().x;
                        var dot_y = dot[0].getBoundingClientRect().y;
                        var mouse_x = e.clientX;
                        var mouse_y = e.clientY;
                        if ((mouse_x >= (dot_x - 50) && mouse_x <= (dot_x + 50)) && (mouse_y >= (dot_y - 50) && mouse_y <= (dot_y + 50))) {
                            if (!dot.hasClass("hover")) {
                                dot.addClass("hover");
                                TweenMax.to(dot[0], .2, {x: random_x, y: random_y});
                            }
                        } else {
                            if (dot.hasClass("hover")) {
                                dot.removeClass("hover");
                                TweenMax.to(dot[0], .2, {x: 0, y: 0});
                            }
                        }
                    });
                });
            });
        }

        //Team Circle Rotate
        $(".shape-3 .image-container").hover(function () {
            var circle = $(this).next(".blue-circle");
            TweenMax.to(circle[0], 5, {rotation:360, ease: Linear.easeNone, repeat: -1});
        }, function () {
            var circle = $(this).next(".blue-circle");
            TweenMax.to(circle[0], 1, {rotation:0, ease: Linear.easeNone});
        });

        //Team Bio
        if($(window).width() <= 1024) {
            $(".bio-button").attr("aria-expanded", false);
            $(".bio").attr("aria-hidden", true);
        }

        $(window).on("resize", function () {
            if($(window).width() <= 1024) {
                $(".bio-button").attr("aria-expanded", false);
                $(".bio").attr("aria-hidden", true);
            } else {
                $(".bio-button").attr("aria-expanded", true);
                $(".bio").attr("aria-hidden", false);
                $(".bio").removeClass("show");
            }
        });

        $(".bio-button").on("click", function () {
            var button = $(this);
            var bio = button.next();
            bio.toggleClass("show");
            if(bio.hasClass("show")){
                bio.slideDown();
                button.text("Collapse Bio >");
            } else {
                bio.slideUp();
                button.text("Read Bio >");
            }
        });
    }
}