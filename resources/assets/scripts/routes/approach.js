import "waypoints/lib/jquery.waypoints.min.js";

export default {
    init() {

    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS
        $(".usage-item .toggle").on("click", function () {
            var toggle = $(this);
            var item = toggle.parents(".usage-item");
            var content = item.find(".row");
            toggle.toggleClass("open");
            if (toggle.hasClass("open")) {
                toggle.attr("aria-expanded", true);
                toggle.html("Collapse");
                content.slideDown({
                    start: function () {
                        $(this).css({
                            display: "flex"
                        })
                    }
                });
                content.attr("aria-hidden", false);
            } else {
                toggle.attr("aria-expanded", false);
                toggle.html("Learn More");
                content.slideUp();
                content.attr("aria-hidden", true);
            }
        });

        if ($(".intro .shapes").length > 0) {
            $(".intro .shapes").each(function () {
                var shapes = $(this);
                var inner = shapes.find(".shapes-inner");
                var shapeWaypoint = new Waypoint({
                    element: shapes[0],
                    handler: function () {
                        inner.addClass("show");
                    },
                    offset: "75%",
                });
            })
        }

        //circle hover
        $(".shapes .circle").on("mouseenter", function () {
            $(this).toggleClass("orange");
        });

        //people hover 1
        $(".people-container-1 .person .head, .people-container-1  .person .torso, .people-container-1  .person" +
            " .legs").on("mouseenter", function () {
            if ($(this).hasClass("legs")) {
                $(this).children().toggleClass("switch");
            } else {
                $(this).toggleClass("switch");
            }
        });

        //people hover 2
        $(".people-container-2 .person .head").on("mouseenter", function () {
            if ($(this).hasClass("peach")) {
                $(this).removeClass("peach");
                $(this).addClass("blue");
            } else if ($(this).hasClass("blue")) {
                $(this).removeClass("blue");
                $(this).addClass("orange");
            } else if ($(this).hasClass("orange")) {
                $(this).removeClass("orange");
                $(this).addClass("black");
            } else if ($(this).hasClass("black")) {
                $(this).removeClass("black");
                $(this).addClass("peach");
            }
        });

        //trend hover
        $(".trend").hover(function () {
            $(this).toggleClass("hover");
        });

        //buttons hover
        $(".button-graphic").hover(function () {
            $(this).toggleClass("pressed");
        });

        $(".resize-shape").on("mouseenter", function () {
            if (!$(this).hasClass("grow")) {
                $(".resize-shape").toggleClass("grow");
            }
        });

        //scalar hover
        $(".scalar-container .circle").on("mouseenter", function () {
            if(!$(this).hasClass("active")) {
                $(this).parent().find(".circle").removeClass("active");
                $(this).addClass("active");
            }
        });

        //sampling graphic
        $(".sampling-graphic").hover(function () {
            $(this).toggleClass("active");
        })
    },
};
