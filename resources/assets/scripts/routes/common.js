import "waypoints/lib/jquery.waypoints.min.js";

export default {
    init() {
        //Mouse Cursor
        $("body").removeClass("no-js");
        var mouse_cursor = $("#cursor");

        function mouseX(e) {
            return e.clientX;
        }

        function mouseY(e) {
            return e.clientY;
        }

        function positionCursor(e) {
            var mouse = {
                x: mouseX(e),
                y: mouseY(e)
            };
            mouse_cursor.css({
                top: mouse.y + 'px',
                left: mouse.x + 'px'
            });
        }

        $(window).on("mousemove", function (e) {
            var event = e;
            var time = setTimeout(function () {
                positionCursor(event);
            })
        });

        $(document).find("a, button, input").hover(function () {
            $("#cursor").addClass("hover");
        }, function () {
            $("#cursor").removeClass("hover");
        });

        //Responsive Topnav
        var windowWidth = $(window).width();
        if (windowWidth <= 1024) {
            $("#topnav").attr("aria-hidden", true);
            $("#menu-button").attr("aria-expanded", false);
            $("#topnav button, #topnav a").attr("tabindex", -1);
        }

        $(window).on("resize", function () {
            windowWidth = $(window).width();
            if (windowWidth > 1024) {
                $("#topnav").attr("aria-hidden", false);
                $("#topnav").addClass("show");
                $("#menu-button").attr("aria-expanded", true);
                $("body").removeClass("menu-open");
                $(".topnav-button").removeClass("open");
                $("#topnav button, #topnav a").attr("tabindex", 0);
            } else {
                $("#topnav").attr("aria-hidden", true);
                $("#topnav").removeClass("show");
                $("#menu-button").attr("aria-expanded", false);
                $(".topnav-button").removeClass("open");
                $("#topnav button, #topnav a").attr("tabindex", -1);
            }
        });

        $(".topnav-button").on("click", function () {
            $(".topnav-button").toggleClass("open");
            if ($(this).hasClass("open")) {
                $(".topnav-button").attr("aria-expanded", true);
                $("#topnav").attr("aria-hidden", false);
                $("#topnav").addClass("show");
                $("body a, body button").not("#topnav a, #topnav button").attr("tabIndex", -1);
                $("#topnav button, #topnav a").attr("tabindex", 0);
                $("#menu-close").focus();
            } else {
                $(".topnav-button").attr("aria-expanded", false);
                $("#topnav").attr("aria-hidden", true);
                $("#topnav").removeClass("show");
                $("body a, body button").not("#topnav a, #topnav button").attr("tabIndex", 0);
                $("#topnav button, #topnav a").attr("tabindex", -1);
                $("#menu-button").focus();
            }
        });

        //Gravity Form Submit focus
        $(".gform_wrapper input[type=submit]").on("focus", function () {
            $(this).parent().addClass("focus");
        });

        $(".gform_wrapper input[type=submit]").on("blur", function () {
            $(this).parent().removeClass("focus");
        });

        $(".gform_wrapper input[type=submit]").hover(function () {
            $(this).parent().addClass("hover");
        }, function () {
            $(this).parent().removeClass("hover");
        });

        //Lazy Load Images
        if ($("[data-src]").length > 0) {
            $("[data-src]").each(function () {
                var img = $(this);
                var imgWaypoint = new Waypoint({
                    element: img[0],
                    handler: function () {
                        if (img.attr("src") === "") {
                            var src = img.attr("data-src");
                            img.attr("src", src);
                            if (isIE()) {
                                if (img.parent().hasClass("image-container")) {
                                    var container = img.parent();
                                    container.addClass("ie");
                                    centerImage(container);
                                }
                            }
                        }
                    },
                    offset: "110%",
                });
            })
        }

        //IE Issues
        function isIE() {
            var ua = window.navigator.userAgent; //Check the userAgent property of the window.navigator object
            var msie = ua.indexOf('MSIE '); // IE 10 or older
            var trident = ua.indexOf('Trident/'); //IE 11

            return (msie > 0 || trident > 0);
        }

        if (isIE()) {
            $("html").addClass("ie");
        }

        function centerImage(imageContainer) {
            //set size
            var th = imageContainer.height(),//box height
                tw = imageContainer.width(),//box width
                im,//image
                ih,//initial image height
                iw;//initial image width
            if (imageContainer.children('img, iframe, video, picture').length > 0) {
                im = imageContainer.children('img, iframe, video');
                ih = im.height();
                iw = im.width();
            }

            if (im !== undefined) {
                if ((th / tw) > (ih / iw)) {
                    im.addClass('wh').removeClass('ww');//set height 100%
                } else {
                    im.addClass('ww').removeClass('wh');//set width 100%
                }

                //set offset
                var nh = im.height(),//new image height
                    nw = im.width(),//new image width
                    hd = (nh - th) / 2,//half dif img/box height
                    wd = (nw - tw) / 2;//half dif img/box width
                if (hd < 1) {
                    hd = 0;
                }
                if (wd < 1) {
                    wd = 0;
                }

                im.css({marginLeft: '-' + wd + 'px', marginTop: '-' + hd + 'px'});//offset left
            }
        }
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        //pinwheel hover
        $(".pinwheel").hover(function () {
            TweenMax.to($(this)[0], 4, {rotation: 360});
        }, function () {
            TweenMax.to($(this)[0], 1, {rotation: 0});
        });

        //pill hover
        $(".pill").on("mouseenter", function () {
            $(this).toggleClass("orange");
        });

        //form active state
        $(".gform_wrapper input,.gform_wrapper textarea").not("input:submit").on("keyup", function () {
            var value = $(this).val();
            if (value !== "") {
                $(this).addClass("active");
            } else {
                $(this).removeClass("active");
            }
        });
    },
};
