export default {
    init() {

    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS
        //toggle orange
        $(".toggle").on("mouseenter", function () {
            $(this).toggleClass("switch");
        });

        //plot graph hover
        if ($(".plot-graph").length > 0) {
            $(".plot-graph").each(function () {
                var graph = $(this);

                graph.find("ellipse").each(function () {
                    var dot = $(this);
                    var random_x = Math.floor(Math.random() * 5) + 5;
                    random_x *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;
                    var random_y = Math.floor(Math.random() * 5) + 5;
                    random_y *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;

                    $(document).on("mousemove", function (e) {
                        var dot_x = dot[0].getBoundingClientRect().x;
                        var dot_y = dot[0].getBoundingClientRect().y;
                        var mouse_x = e.clientX;
                        var mouse_y = e.clientY;
                        if ((mouse_x >= (dot_x - 30) && mouse_x <= (dot_x + 30)) && (mouse_y >= (dot_y - 30) && mouse_y <= (dot_y + 30))) {
                            if (!dot.hasClass("hover")) {
                                dot.addClass("hover");
                                TweenMax.to(dot[0], .2, {x: random_x, y: random_y});
                            }
                        } else {
                            if (dot.hasClass("hover")) {
                                dot.removeClass("hover");
                                TweenMax.to(dot[0], .2, {x: 0, y: 0});
                            }
                        }
                    });
                });
            });
        }
    },
};
