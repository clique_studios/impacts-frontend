export default {
    init() {
        if($("#blog img").length > 0){
            $("#blog img").each(function () {
                $(this).parent().addClass("image-container");
            })
        }
    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS

    },
};
