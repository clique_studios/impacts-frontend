export default {
    init() {

    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS
        //Section Anchor
        function scrollToAnchor(aid) {
            var aTag = $(aid);
            var focus = true;
            $('html,body').animate({scrollTop: aTag.offset().top - 50}, 'slow');
            if(aTag.find("a:visible, button:visible, input:visible").length > 0){
                $(aTag.find("a:visible, button:visible, input:visible")[0]).focus();
            }
        }

        $(".anchors a").on("click", function (e) {
            e.preventDefault();
            scrollToAnchor($(this).attr("href"));
        });

        //Accordion Toggle
        $(".toggle").on("click", function () {
            $(this).toggleClass("open");
            var parent = $(this).parent();
            var cnt = parent.find(".row");
            if($(this).hasClass("open")){
                $(this).attr("aria-expanded", true);
                $(this).html("Collapse");
                cnt.attr("aria-hidden", false);
                cnt.slideDown({
                    start: function () {
                        $(this).css({
                            display: "flex"
                        })
                    }
                })
            } else {
                $(this).attr("aria-expanded", false);
                $(this).html("Expand");
                cnt.attr("aria-hidden", true);
                cnt.slideUp();
            }
        })
    },
};
